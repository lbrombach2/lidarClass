#include "lidar.h"

using namespace std;

lidar::lidar()
{
    //ctor
    scan();


}
/*
lidar::~lidar()
{
    //dtor
}*/
int lidar::scan(){
    cycles=0;
    int startTime=time(NULL);

    for (int i=0;i<360;i++){ //for each read, shift previous reading up the array from 0-2
        dataArr[i][2]=dataArr[i][1];
        dataArr[i][1]=dataArr[i][0];
        dataArr[i][0]=0; //reset 0 back to 0 each scan
        dataArr[i][3]=0;} //rest 3 back to 0 each scan

    int lidarOn=17;
    int rx=18;
    if(gpioInitialise() < 0){cout << "NOOOOPE Try again" << endl;}
    gpioSetMode(lidarOn, PI_OUTPUT);
    gpioSetMode(rx, PI_INPUT);
    gpioWrite(lidarOn,0); //0=lidar powered and on
    gpioSleep(PI_TIME_RELATIVE,3,2000); //time for lidar to spin up



while(full()==false&&cycles<20){

    cycles++;
	gpioSleep(PI_TIME_RELATIVE,0,100000);  //10ms delay
    if(gpioSerialReadOpen(rx,115200,8)!=0){cout<<"unable to open bitbang"<<endl;
    }else{ //	cout<<"bitbang open ok"<<endl;
        char buf[bufSize];
	for(int i=0; i < bufSize; i++) buf[i]=0;
        gpioSleep(PI_TIME_RELATIVE,1,10);  //10ms delay
 	cout<<"reading this many int buf[] - "<<gpioSerialRead(rx, buf, bufSize)<<endl;
	gpioSerialRead(rx, buf, bufSize);
        gpioSerialReadClose(rx);

    for(int i=0; i < bufSize; i++){ //identify packet starts and send to checksum
        if(buf[i]==250){
            int packet[22];
            for (int j=0;j<22;j++){
            packet[j]=static_cast<unsigned int>(buf[i+j]);//comes out as char, need to send int
            }

            if(legit(packet)){//tests checksum and adds to dataArr[] if legit
        //stick(packet);//add packet to matrix to compare to other samples and determine best?
            }
        }
    }
}

        }

    gpioWrite(lidarOn,1); //1 = lidar relay off

    gpioTerminate(); //Always use at end of program to release memory and DMA channels

   // for (int i=0;i<360;i++){
   //  cout<<"Heading "<<i<<"  dis "<<dataArr[i][0]<<   "   str "<<dataArr[i][3]<<endl;
   //  }
cout<<"##########TOOK "<<time(NULL)-startTime<<" seconds########"<<endl;
    return 0;
}

bool lidar::legit(int packet[]){

int txDchksum = (packet[20] + (packet[21] << 8));//combine checksum bytes to get transmitted checksum

for(int i=0;i<10;i++){
checksumPacket[i]=(packet[(2*i)]+ (packet[2*i+1]<<8)); //calculate checksum on rest of packet
}
int chk32=0;
for(int i=0;i<10;i++){
chk32=(chk32<<1)+checksumPacket[i];
}
    int checksum;
    checksum = (chk32 & 0x7FFF) + ( chk32 >> 15 ); // wrap around to fit into 15 bits
    checksum = checksum & 0x7FFF; // truncate to 15 bits




	if (checksum==txDchksum && txDchksum!=0){
		int theta=((packet[1]-160)*4)+90;
		if (theta>359)theta-=360;
		int speed = checksumPacket[1];
		int d1Dis = checksumPacket[2];
		int d1Str = checksumPacket[3];
		int d1Theta = theta;
		int d2Dis = checksumPacket[4];
		int d2Str = checksumPacket[5];
		int d2Theta = theta+1;
		int d3Dis = checksumPacket[6];
		int d3Str = checksumPacket[7];
		int d3Theta = theta+2;
		int d4Dis = checksumPacket[8];
		int d4Str = checksumPacket[9];
		int d4Theta = theta+3;
	    if(d1Str>dataArr[d1Theta][3]){ //add extra error detecting/averaging here?
		dataArr[d1Theta][0]=d1Dis;
		dataArr[d1Theta][3]=d1Str;
	    }
	    if(d2Str>dataArr[d4Theta][3]){ //add extra error detecting/averaging here?
		dataArr[d2Theta][0]=d2Dis;
		dataArr[d2Theta][3]=d2Str;
	    }
	    if(d3Str>dataArr[d4Theta][3]){ //add extra error detecting/averaging here?
		dataArr[d3Theta][0]=d3Dis;
		dataArr[d3Theta][3]=d3Str;
	    }
	    if(d4Theta==360){d4Theta=0;}
	    if(d4Str>dataArr[d4Theta][3]){ //add extra error detecting/averaging here?
		dataArr[d4Theta][0]=d4Dis;
		dataArr[d4Theta][3]=d4Str;
	    }

return true;
}
else return false;
}

bool lidar::full(){ //checks to see how many points out of 360 we've acquired
int tally=0;
 int percent =0;
    for (int i=0;i<360;i++){
	if (dataArr[i][0]!=0) tally++;
    }
   percent =(tally*100)/360;
    cout<<"Starting read cycle "<<cycles+1<<"    ";
    cout<<percent<< "% of points acquired"<<endl;
if(percent>80) return true;  //Through some trial and error, I picked 65% for a "best return on time waited" Still testing
else return false;
}

int lidar::getDis(int bearing){
if (dataArr[bearing][3]>20){
return dataArr[bearing][0];
}
//else cout<<"0. Last 2 scans were "<<dataArr[bearing][1] <<" and "<<dataArr[bearing][2]<<"      ";
//else cout<<dataArr[bearing][0]<<"   "<<dataArr[bearing][1] <<"   "
//<<dataArr[bearing][2]<<"             ";

return 0;
}



