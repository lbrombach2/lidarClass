This is my first c++ class for using the xv11 neato robot vacuum's lidar unit with a raspberry pi 3. 
The class is intended to be integrated with autonomous mapping and navigation files and ultimately, an entire robot program.
You can run this git project as a standalone program(with main.cpp) for testing...there are some extra cout statements and optional variables in the program for testing purposes.
Or you can add just the lidar.cpp and lidar.h to your c++ project and compile with -lpigpio (I use the pigpio library). Set up this way, 
lidar does not run continuously and not intended to be running while a robot or whatever is in 
motion - samples are taken and written to an array when scan() is called. You could modify just a little and run it continuously in a thread, but you'd need to 
add some kind of mutex or atomic protection so your program doesn't try to read and write to a memory location at the same time. 

When you create an object (I used 'ranges') in the lidar class, void scan() is automatically run to spin up the lidar unit and grab the first data.
Then call getDis(int degreeheadingyouwantthedistanceof) in that object (" ranges.getDis(int) ") and it will return the distance stored from the last 
set of readings. Serial bitbanging seems to decrease the data quality quite a bit, but my physical UART is taken.  I usually only get 50-60%
of the points read per loop (that includes a number of cycles). Just call object.scan()/(ranges.scan() ) to try again if you really 
need a specific point. I don't bother sending commands to the unit becuase it starts sending data as soon as it gets up to speed.

Also note that I use a unit circle style where 0 degrees is to the right of my robot(and the front of the lidar) and 
they increase counter-clockwise, instead of the standard cartography clockwise increase. I've got a few more tidbits and advice at
https://lloydbrombach.wordpress.com/2018/08/24/reading-the-xv11-neato-botvac-lidar-with-the-raspberry-pi-and-c/

That's all I can think of for the moment, you can see more about my autonomous robot project or contact me with questions at 
https://lloydbrombach.wordpress.com/ or more regular, but less detailed tidbits on the Autonomous Robot Builders and Programmers 
Facebook Group https://www.facebook.com/groups/273792653417570/



