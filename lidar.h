#ifndef LIDAR_H
#define LIDAR_H

#include <iostream>


#include <cstdio>
#include <cstdlib>
#include <pigpio.h>
#include<time.h>

class lidar
{
    public:
        lidar();//constructor
        int scan();//spools up lidar and grabs raw data for processing
        int getDis(int bearing);
//        virtual ~lidar


    protected:

    private:
	const int bufSize=2000;
        int lidarOn=17;
    	int rx=18;
	int checksumPacket[10]={0};
	int dataArr[360][4]={0};
	int cycles =0;

    	bool legit(int packet[]); //tests checksum
	bool full();

};

#endif // LIDAR_H
