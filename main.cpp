#include <iostream>
#include "lidar.h"


using namespace std;

int main()
{
    cout << "Welcome to Lidar Interface" << endl<<endl
    <<"The purpose is to develop an useable class to return lidar data"<<endl
    <<"main.cpp realy only exists to simulate another program that might call the class"<<endl<<endl;


    int heading = -1;


    cout<< endl<<"Starting Lidar Scan"<<endl;

    lidar ranges;
for (int i=359;i>=0;i--){

cout<< " bearing "<<i<<"  distance "<<ranges.getDis(i)<<endl;}
cout<<endl;

while(1){

    heading = -1;

    cout<<endl<<"Enter a degree heading to read from 1-360 (Unit circle degrees where straight ahead = 90"<<endl;
    cout<<"Or enter 555 to rescan for fresh data, or 999 to exit program"<<endl;
    while(heading <0 ||heading>359){
    cin>>heading;
	if(heading==999){return 0;}//exit program

    if (heading==555){
	cout<<"Starting Lidar Scan"<<endl;
        ranges.scan();
	for (int i=359;i>=0;i--){
	cout<< " bearing "<<i<<"  distance "<<ranges.getDis(i)<<endl;}
    cout<<endl<<"Enter a degree heading to read from 1-360 (Unit circle degrees where straight ahead = 90"<<endl;
    cout<<"Or enter 555 to rescan for fresh data, or 999 to exit program"<<endl;
    }
    cout<<endl;
}

    cout<< endl<<"Requested distance at "<<heading<<" = "<<ranges.getDis(heading)<<endl<<endl;
    heading=-1;
}

    return 0;
}
